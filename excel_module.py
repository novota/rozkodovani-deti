import data_classes
from openpyxl.styles import Font, PatternFill


# goes through the 1st line from A to y of a worksheet and returns the letter of column with column name
def get_column_letter(ws, column_name):
	letter = 65  # 65 is ascii for A
	index = 1  # which row we go through
	go_on = True
	while go_on:
		cell_idx = chr(letter) + str(index)  # creates A1, B1 and so on
		curr_value = ws[cell_idx].value.lstrip().rstrip()
		if curr_value == column_name:
			return letter
		letter += 1  # try next letter
		if chr(letter) == 'Z':
			go_on = False
	return None


# localizes collumn with column_name and extracts all values until None comes, returns python list of data
def get_data_from_column(ws, column_name, length=0):
	column_letter = get_column_letter(ws, column_name)
	result = []
	if column_letter is None:
		print('no column found with given column_name')
		return None
	index = 2
	go_on = True
	while go_on:
		cell_idx = chr(column_letter) + str(index)  # create current cell index eg. A10
		curr_data = ws[cell_idx].value
		if curr_data is None and ws[chr(column_letter + 1) + str(index)].value is None and length == 0:
			go_on = False
		else:
			if curr_data is None:
				curr_data = "-1"
			result.append(curr_data)
			index += 1
			if length != 0:
				length -= 1
	return result


def save_data_to_column(data: list, ws, start_row: int, column: int):
	"""Store 1D data from data array into target column starting at given row.

	:param data: 1D list of data
	:param start_row: integer describing first row where data will be written
	:param column: column letter given as ASCII code describing column in which data shall be written
	:param ws: work sheet where data shall be written
	:return: None
	"""
	column_matching_res = column - 1
	ws[chr(column_matching_res) + str(start_row - 1)].value = "Presnost"
	column_f_code = column

	for kid in data:
		if kid.friends_matched is not data_classes.FriendsWishResult.MATCHING_NOT_DONE_YET:
			f_code_cell = ws[chr(column_f_code) + str(start_row)]
			matching_res_cell = ws[chr(column_matching_res) + str(start_row)]
			f_code_cell.value = kid.f_code
			worst_case = data_classes.Matching.PERFECT_MATCH

			# sort friends from worst result to the best so that i can choose font color according to the worst result
			for touple in kid.list_of_friends_i_want:
				if touple[1] < worst_case:
					worst_case = touple[1]

			if kid.friends_matched == data_classes.FriendsWishResult.NO_MATCH_FOUND:
				worst_case = data_classes.Matching.MATCH_NOT_FOUND
				matching_res_cell.fill = PatternFill("solid", fgColor="00FF0000")

			elif worst_case == data_classes.Matching.NO_ACCENTS:
				matching_res_cell.fill = PatternFill("solid", fgColor="0000FFFF")
			elif worst_case == data_classes.Matching.MORE_THEN_EIGHTY_PERCENT:
				matching_res_cell.fill = PatternFill("solid", fgColor="0099CC00")
			elif worst_case == data_classes.Matching.FIFTY_TO_EIGHTY_PERCENT:
				matching_res_cell.fill = PatternFill("solid", fgColor="00FF9900")
			elif worst_case == data_classes.Matching.INITIAL_ONLY:
				matching_res_cell.fill = PatternFill("solid", fgColor="00FF00FF")
			elif worst_case == data_classes.Matching.LESS_THEN_FIFTY_PERCENT:
				matching_res_cell.fill = PatternFill("solid", fgColor="00FF660")
			elif worst_case == data_classes.Matching.MATCH_NOT_FOUND:
				matching_res_cell.fill = PatternFill("solid", fgColor="00FF0000")

			matching_res_cell.value = str(worst_case)

		start_row += 1
