import data_classes
import Trida_dite
import typing
import unidecode
import string_module


def convert_to_no_accents(kids: list):
	result = []
	for idx, dict in enumerate(kids):
		result.append({})
		for key in dict:
			result[idx][unidecode.unidecode(key)] = dict[key]
	return result


def get_seznam_deti(kids: list) -> list:
	result = []
	for kid in kids:
		result.append(Trida_dite.Dite(name=kid.lstrip().rstrip()))
	return result


def get_possible_name(string_position_last_looked_at: int, desired_friend: str) -> typing.Union:
	"""Find a string between index where last search stopped and next non alphabetical character.

	:param string_position_last_looked_at: index in the string where last search stopped
	:param desired_friend: string containing all names a kid wishes to be together with
	:return: index where search stopped and string which represents a possible name
	"""
	go_on = True
	start_idx = -1
	last_idx = -1
	while go_on and string_position_last_looked_at < len(desired_friend):
		# find first letter
		for index in range(string_position_last_looked_at, len(desired_friend)):
			if desired_friend[index].isalpha():
				start_idx = index
				break

		if start_idx == -1:
			return None, len(desired_friend)

		# find last letter
		for index in range(start_idx, len(desired_friend)):
			if not desired_friend[index].isalpha():
				last_idx = index
				break
		# case where there are letters until the end
		if last_idx == -1:
			last_idx = len(desired_friend)
		go_on = False

	return desired_friend[start_idx: last_idx].capitalize(), last_idx


def get_all_words_from_string(reference_name: str):
	"""Get list of all words from a string.

	:param reference_name: string which doesnt start or ends with a whitespace
	:return: list of all words found in reference name
	"""
	result = []
	indexes_separating_words = [n for n in range(len(reference_name)) if reference_name.find(" ", n) == n]
	indexes_separating_words.append(0)
	indexes_separating_words.append(len(reference_name))
	indexes_separating_words.sort()

	for i in range(len(indexes_separating_words) - 1):
		result.append(reference_name[indexes_separating_words[i]: indexes_separating_words[i + 1]].rstrip().lstrip().capitalize())

	return result


def match_names(first_name: str, reference_name: str) -> data_classes.Matching.PERFECT_MATCH:
	""" Compare two names and determine the likelihood that they are the same.

	:param first_name: this is the input string which will be compared to the reference name
	:param reference_name: the first name is compared to this
	:return: Likelihood that fist name is the same as reference name
	"""
	# check preconditions
	assert len(first_name) != 0
	assert len(reference_name) != 0

	# if there are multiple first names, separate them and try them all
	list_of_first_names = get_all_words_from_string(reference_name)
	if len(list_of_first_names) > 1:
		best_match = data_classes.Matching.NO_MATCH
		for name in list_of_first_names:
			tmp_match = match_names(first_name, name)
			best_match = tmp_match if tmp_match > best_match else best_match

		return best_match

	if first_name == reference_name:
		return data_classes.Matching.PERFECT_MATCH

	# try removing accents
	first_name = unidecode.unidecode(first_name)
	reference_name = unidecode.unidecode(reference_name)
	if first_name == reference_name:
		return data_classes.Matching.NO_ACCENTS

	# if first name is just one letter, it is an initial and it must be the same as the first letter in reference name
	if len(first_name) == 1 and first_name == reference_name[0]:
		return data_classes.Matching.INITIAL_ONLY

	# first letter must be the same or else names are not the same
	if first_name[0] != reference_name[0]:
		return data_classes.Matching.NO_MATCH

	# if first letter matches, look at the rest and count same letters
	shorter_name_length = min(len(first_name), len(reference_name))
	number_of_matching_letters = 0
	for i in range(1, shorter_name_length):
		if first_name[i] == reference_name[i]:
			number_of_matching_letters += 1

	result_match = number_of_matching_letters / shorter_name_length
	if result_match > 0.8:
		return data_classes.Matching.MORE_THEN_EIGHTY_PERCENT
	if result_match > 0.5:
		return data_classes.Matching.FIFTY_TO_EIGHTY_PERCENT
	return data_classes.Matching.LESS_THEN_FIFTY_PERCENT


def separate_name_index(string: str):
	"""Separate name and index.

	:param string:  is of the form "name!index"
	:return: name and index
	"""
	return string[0:string.find("!")], int(string[string.find("!") + 1:])


def matching_name_found(dictionary_indexes_first_names: list):
	"""Go through list of matching results and determine whether a good enough match was found.

	:param dictionary_indexes_first_names: Sorted list of matching results. Its a list of tuples of the form (Matching, last name, first name, index in seznam_deti)
	:return: True if the first list entry is a good enough match, False otherwise
	"""
	# empty list
	if not dictionary_indexes_first_names:
		return False
	# bad match
	if dictionary_indexes_first_names[0][0] < data_classes.Matching.INITIAL_ONLY:
		return False
	# two or more entries with same matching result
	if len(dictionary_indexes_first_names) > 1 and dictionary_indexes_first_names[0][0] == dictionary_indexes_first_names[1][0]:
		return False
	# if none of the above conditions are met, the matching is good enough
	return True


def match_desired_friends_to_kids(seznam_deti: list, kids: list, desired_friends: list, precision_level: data_classes.Matching) -> list:
	"""Take the desired friends list and figure out for each entry, which actual kid matches the best with it.

	:param seznam_deti: list with elements of type Dite where all kids are saved and where recognized friends are stored
	:param kids: list of dictionaries derived from seznam deti, keys are last names and values are first names. This permits faster search speed
	:param desired_friends: list of strings which represent a wish for maximal two friends. A kid from seznam_deti with index i has a wish in desired_friends list at index i as well
	:param precision_level: represents the precision level of matching
	:return: list of string in which no kid was recognized
	"""
	print("matched kids")
	unmatched_friends = []
	for i, desired_friend in enumerate(desired_friends):
		if desired_friend == "-1":
			continue

		first_name = ""  # this is first name if the friends input is ordered like: first name, last name, e.g. Jakub Novak
		string_position_last_looked_at = 0
		matched = False
		match_counter = 0
		curr_kid = seznam_deti[i]
		curr_kid.friends_matched = data_classes.FriendsWishResult.NO_MATCH_FOUND

		while string_position_last_looked_at < len(desired_friend):
			last_name_candidate, string_position_last_looked_at = get_possible_name(string_position_last_looked_at, desired_friend)
			dictionary_indexes_first_names = []

			# find dictionaries where the last name is present
			for index, dict_of_kids in enumerate(kids):
				if last_name_candidate in dict_of_kids:
					# if first
					first_name_reference, index_seznam_deti = separate_name_index(dict_of_kids[last_name_candidate])
					dictionary_indexes_first_names.append((last_name_candidate, first_name_reference, index_seznam_deti))  # store (last name, first name, index_in_seznam_deti)
				else:
					break
			# match first name
			# TODO: if first name is not matched we could try to look if first name comes after the last name.
			for index, union in enumerate(dictionary_indexes_first_names):
				# if first name is empty, that means first word is last name and first name must be the second word
				if not first_name:
					first_name, tmp = get_possible_name(string_position_last_looked_at, desired_friend)

				names_similarity = match_names(first_name, union[1])  # name similarity is a percentage which represents the likelihood that 2 Names are the same
				dictionary_indexes_first_names[index] = (names_similarity, union[0], union[1], union[2])  # (Matching, last name, first name, index in seznam_deti)

			# sort results from most likely to least likely
			dictionary_indexes_first_names.sort(reverse=True)

			# safe the result
			if matching_name_found(dictionary_indexes_first_names):
				match_counter += 1
				desired_friends[i] = "-1"  # mark as -1 so that we know this wish has been fulfilled
				matched = True
				curr_kid.friends_matched += 1
				curr_kid.list_of_friends_i_want.append((seznam_deti[dictionary_indexes_first_names[0][3]], dictionary_indexes_first_names[0][0]))  # (kid, Matching)
				seznam_deti[dictionary_indexes_first_names[0][3]].list_of_people_who_want_me.append((curr_kid, dictionary_indexes_first_names[0][0]))  # (kid, Matching)
				print(f"{desired_friend} matched to {seznam_deti[dictionary_indexes_first_names[0][3]]}")

			else:
				match_counter -= 1

			first_name = last_name_candidate

		# match counter is not zero that means we did not recognize all words and there is uncertainty about the result
		if match_counter == -1 or match_counter == -3 or match_counter == -5:
			print(f"{-match_counter} words from wish list have not been recognized as friends name, mabye we need to deal with this situation maybe not. Well see. I bet we will have to deal with it.")

		if not matched:
			unmatched_friends.append(desired_friend)
			print(f"{desired_friend} was not matched to anybody")

	return desired_friends


def get_out_path(input_path, new_appendix):
	output_path = ''
	for char in input_path:
		if char == ".":
			break
		output_path += char
	output_path += new_appendix
	return output_path
