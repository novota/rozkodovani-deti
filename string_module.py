def is_letter(character):
	return 64 < ord(character) < 91 or 96 < ord(character) < 123 or ord(character) == 138 or ord(character) == 142 or ord(character) == 154 or ord(character) == 158\
	       or 191 < ord(character) < 215 or 215 < ord(character) < 247 or 247 < ord(character) < 256