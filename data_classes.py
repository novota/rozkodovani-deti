from enum import Enum
from openpyxl.styles import Font

# accents
class Matching(Enum):
	"""Defines the level of matching precision."""

	PERFECT_MATCH = 10
	NO_ACCENTS = 9
	MORE_THEN_EIGHTY_PERCENT = 8
	FIFTY_TO_EIGHTY_PERCENT = 7
	INITIAL_ONLY = 6
	LESS_THEN_FIFTY_PERCENT = 5
	NO_MATCH = 4
	MATCH_NOT_FOUND = 3

	def __lt__(self, other):
		return self.value < other.value

	def __str__(self):
		if self == Matching.PERFECT_MATCH:
			return "Shoda"
		if self == Matching.NO_ACCENTS:
			return "Bez Diakritiky"
		if self == Matching.MORE_THEN_EIGHTY_PERCENT:
			return "Vice jak 80%"
		if self == Matching.FIFTY_TO_EIGHTY_PERCENT:
			return "50% - 80%"
		if self == Matching.INITIAL_ONLY:
			return "Shodne inicialy"
		if self == Matching.LESS_THEN_FIFTY_PERCENT:
			return "mene nez 50%"
		if self == Matching.NO_MATCH:
			return "Zadna shoda"
		if self == Matching.MATCH_NOT_FOUND:
			return "Shoda nenalezena"
		return "Neznamy typ"


class MatchingFonts(Enum):
	"""Defines colors for excel output. Colers descripe the level of certainty that the matching is correct."""

	NO_ACCENTS = Font(color="00FFFFCC")
	MORE_THEN_EIGHTY_PERCENT = Font(color="0000FF00")  # green
	FIFTY_TO_EIGHTY_PERCENT = Font(color="00FFCC00")  # light orange
	INITIAL_ONLY = Font(color="0000FFFF")  # light blue
	LESS_THEN_FIFTY_PERCENT = Font(color="00FF0000")  # red
	MATCH_NOT_FOUND = Font(color="00800080")  # purple




class FriendsWishResult(Enum):
	"""Describes the overall matching result."""

	THREE_MATCHES_FOUND = 3
	TWO_MATCHES_FOUND = 2
	ONE_MATCH_FOUND = 1
	NO_MATCH_FOUND = 0
	DOES_NOT_REQUEST_FRIENDS = -1
	MATCHING_NOT_DONE_YET = -2

	def __iadd__(self, other):
		if self == FriendsWishResult.MATCHING_NOT_DONE_YET:
			return FriendsWishResult.DOES_NOT_REQUEST_FRIENDS
		if self == FriendsWishResult.DOES_NOT_REQUEST_FRIENDS:
			return FriendsWishResult.NO_MATCH_FOUND
		if self == FriendsWishResult.NO_MATCH_FOUND:
			return FriendsWishResult.ONE_MATCH_FOUND
		if self == FriendsWishResult.ONE_MATCH_FOUND:
			return FriendsWishResult.TWO_MATCHES_FOUND
		if self == FriendsWishResult.TWO_MATCHES_FOUND:
			return FriendsWishResult.THREE_MATCHES_FOUND
		if self == FriendsWishResult.THREE_MATCHES_FOUND:
			print("increment does not make sence")
			return self
