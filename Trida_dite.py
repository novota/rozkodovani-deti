import datetime
import data_classes


class Dite:

	def __init__(self, name='none', age=-1, gender='none', rok=-1, month=-1, day=-1, friends_code=-1, oddil=-1, kamarad1=-1, kamarad2=-1):
		self.name = name
		self.age = age
		self.gender = gender
		self.oddil = oddil
		self.chatka = None
		self.kamarad1 = kamarad1
		self.kamarad2 = kamarad2
		self.list_of_friends_i_want = []  # (kid, Matching)
		self.list_of_people_who_want_me = []
		self.friends_matched = data_classes.FriendsWishResult.MATCHING_NOT_DONE_YET
		# TODO: pridat list deti, co si preji toto dite, a pak tu mame list deti, ktere si preje toto dite. Abysme mohli jednoduse trackovat vsechny deti, co k sobe patri
		self.r = rok
		self.m = month
		self.d = day
		self.f_code = friends_code
		self.Mesto = ""
		self.P = ""
		self.Platba = ""
		self.Doplatek = ""
		self.K = ""
		self.W = ""
		self.Kamarad = ""
		self.Pozn_D = ""
		self.Jmeno_R1 = ""
		self.Tel_R1 = ""
		self.Jmeno_R2 = ""
		self.Tel_R2 = ""
		self.poukazy = ""

	def __str__(self):
		if self.oddil == -1:
			result = '{:s} vek {:f} pohlavi {:s} oddil c. {:d} Kod {:d}'.format(self.name, self.age, self.gender, -1, int(self.f_code))
		else:
			result = '{:s} vek {:f} pohlavi {:s} oddil c. {:d} Kod {:d}'.format(self.name, self.age, self.gender, self.oddil.cislo_oddilu, int(self.f_code))
		if self.chatka is not None:
			result += ' chatka c. {:s}'.format(str(self.chatka.cislo_chatky))
		else:
			result += ' bez chatky'
		result += '\n'
		return result


	def pocet_kamaradu(self):
		return int(self.kamarad1 != -1) + int(self.kamarad2 != -1)

	def pocet_kamaradu_stejneho_pohlavi(self):
		result = 1
		if self.kamarad1 != -1 and self.kamarad1.gender == self.gender:
			result += 1
		if self.kamarad2 != -1 and self.kamarad2.gender == self.gender:
			result += 1
		return result

	# returns the oldest friends age who has the same gender
	def vek_nejstersiho_z_kamaradu(self):
		result = self.age
		if self.kamarad1 != -1 and self.kamarad1.age > result and self.kamarad1.gender == self.gender:
			result = self.kamarad1.age
		if self.kamarad2 != -1 and self.kamarad2.age > result and self.kamarad2.gender == self.gender:
			result = self.kamarad2.age
		return result

	def cislo_oddilu(self):
		return int(self.cislo_oddilu())

	def is_boy(self):
		return self.gender == "H"

	def is_girl(self):
		return self.gender == "D"


# funkce ulozi kazdemu objektu dite vsechny jeho kamarady , maximalne dva
def save_friends(seznam_kodu, seznam_deti):
	# projet seznam kodu radek po radku
	for indexR in range(len(seznam_kodu)):
		if seznam_kodu[indexR][0] == -1:  # end loop if there are no more friends codes in the position 0
			break
		# inicializace tri moznych kamaradu
		kamarad1 = None
		kamarad2 = None
		kamarad3 = None
		# ulozit vsechny potencialni kamarady, kamarad 3 muze byt 0
		kamarad1 = seznam_deti[int(seznam_kodu[indexR][1])]

		if not int(seznam_kodu[indexR][2]) == -1:
			kamarad2 = seznam_deti[int(seznam_kodu[indexR][2])]
			kamarad1.kamarad1 = kamarad2
			kamarad2.kamarad1 = kamarad1
		if not int(seznam_kodu[indexR][3]) == -1:
			kamarad3 = seznam_deti[int(seznam_kodu[indexR][3])]
			kamarad1.kamarad2 = kamarad3
			kamarad2.kamarad2 = kamarad3
			kamarad3.kamarad1 = kamarad1
			kamarad3.kamarad2 = kamarad2
