from openpyxl import *
import data_classes
import excel_module
import Help_functions
import Trida_dite
import unidecode


def get_first_and_last_name(name: str):
	name = name.lstrip().rstrip()
	name_splited = name.partition(" ")
	return name_splited[2].lstrip().rstrip().capitalize(), name_splited[0].lstrip().rstrip().capitalize()


def convert_to_first_and_last_names(kids: list) -> list:
	"""Separate first word in a string from the rest and create lists of dictionaries out of it. The first word is the key and for each occurrence of one key create a new dictionary.

	:param kids: list of names where the first word is a last name and the rest is a first name with possible middle name
	:return: list of dictionaries
	"""
	result = [{}]
	for index, name in enumerate(kids):
		first_name, last_name = get_first_and_last_name(name)
		# append index to the first name
		first_name += f"!{index}"
		index_of_dict = -1

		# find if current last name is already in any dictionary, if yes, get the index of the last one
		for idx, dictionary in enumerate(result):
			if last_name in dictionary:
				index_of_dict = idx
			else:
				break
		# kids second name is not saved yet
		if index_of_dict == -1:
			result[0][last_name] = first_name
		else:
			# we need to extend the list by another dictionary
			if index_of_dict + 1 == len(result):
				result.append({})

			result[index_of_dict + 1][last_name] = first_name

	return result


def find_all_matched_friends(kid: Trida_dite.Dite, list_of_all_matched_kids: list):
	for friend_i_want in kid.list_of_friends_i_want:
		if friend_i_want[0] not in list_of_all_matched_kids:
			list_of_all_matched_kids.append(friend_i_want[0])
			find_all_matched_friends(friend_i_want[0], list_of_all_matched_kids)
	for someone_who_wants_me in kid.list_of_people_who_want_me:
		if someone_who_wants_me[0] not in list_of_all_matched_kids:
			list_of_all_matched_kids.append(someone_who_wants_me[0])
			find_all_matched_friends(someone_who_wants_me[0], list_of_all_matched_kids)


def assign_f_code(list_of_all_matched_kids: list, normal_counter: int):
	for kid in list_of_all_matched_kids:
		kid.f_code = normal_counter


def find_all_matching_friends(seznam_deti: Trida_dite) -> None:
	"""Find all kids who want to be together and assign the same code to them. If they are group of more then 3 people, assign them a special code.

	:param seznam_deti: List of Dite objects, each Dite has a list of friends they want and list of people who want to have them.
	:return: None
	"""
	normal_counter = 1
	more_then_three_counter = 500
	for kid in seznam_deti:
		if kid.f_code == -1:
			list_of_all_matched_kids = [kid]
			find_all_matched_friends(kid, list_of_all_matched_kids)
			# assign f codes
			if 1 < len(list_of_all_matched_kids) < 4:
				assign_f_code(list_of_all_matched_kids, normal_counter)
				normal_counter += 1
			elif len(list_of_all_matched_kids) > 3:
				assign_f_code(list_of_all_matched_kids, more_then_three_counter)
				more_then_three_counter += 1


def main():
	file_name = "/Users/petrnovota/Documents/Programming/Test_excel/problemovy_sooubor/N2_problem_tatari.xlsx"
	excel_wb = load_workbook(filename=file_name)
	sheet_names = excel_wb.sheetnames
	list_of_sheet_names = []
	for sheet_name in sheet_names:
		list_of_sheet_names.append(sheet_name)
	print(list_of_sheet_names)

	curr_excel_sheet_name = list_of_sheet_names[0]
	excel_ws = excel_wb[curr_excel_sheet_name]
	kids = excel_module.get_data_from_column(excel_ws, "Jméno")
	seznam_deti = Help_functions.get_seznam_deti(kids)

	nr_of_kids = len(kids)
	kids = convert_to_first_and_last_names(kids)

	# in first attempt we look at perfect last name matches
	desired_friends = excel_module.get_data_from_column(excel_ws, "Kamarád", length=nr_of_kids)
	Help_functions.match_desired_friends_to_kids(seznam_deti, kids, desired_friends, data_classes.Matching.PERFECT_MATCH)

	# in second attempt we look at perfect matches of last names without accents
	kids = Help_functions.convert_to_no_accents(kids)
	desired_friends = [unidecode.unidecode(x) for x in desired_friends]  # remove all accents
	Help_functions.match_desired_friends_to_kids(seznam_deti, kids, desired_friends, data_classes.Matching.PERFECT_MATCH)

	# assign to each group of friends a friends code
	find_all_matching_friends(seznam_deti)

	# save results
	excel_module.save_data_to_column(seznam_deti, excel_ws, 2, excel_module.get_column_letter(excel_ws, "Kód"))
	res_path = Help_functions.get_out_path(file_name, '_kodovani.xlsx')
	excel_wb.save(res_path)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
	main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
